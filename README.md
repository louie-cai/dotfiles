# My Dotfiles

My dotfiles are maintained with [`chezmoi`](https://www.chezmoi.io/quick-start/#start-using-chezmoi-on-your-current-machine). 

# Installation
Install [`chezmoi`](https://www.chezmoi.io/install/).

## On a New Machine
```
chezmoi init --apply https://github.com/username/dotfiles.git
```

## On an Existing Machine
```
chezmoi init https://github.com/username/dotfiles.git
chezmoi diff # check the difference
chezmoi apply -v # apply the changes
```

# Update
```
chezmoi update -v
```
